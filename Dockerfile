FROM rust:1.60 as builder

WORKDIR /usr/src/myapp
COPY hello-world .

RUN cargo install --path .
RUN ls /usr/local/cargo/bin

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/app /usr/local/bin/hello-world
EXPOSE 8080
USER nobody
CMD ["hello-world"]
